/*
    mercury.c
    OpenGL simulation of orbit of planet Merury.
    Tested on Linux.
    
    (C)Copyright 2017 Eric Shalov.
*/

#include <string.h>
#include <stdlib.h>
#ifdef MACOSX
#include <util.h>
#endif
#include <stdio.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/resource.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <regex.h>
#include <ctype.h>
#include <sys/stat.h>
#include <math.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#ifdef MACOSX
#include <GLUT/segment.h>
#endif

#include <png.h>
#include "jpeglib.h"

#include "texture.h"

/* prototypes */
void load_texture(GLuint *texture_id);
struct planetoid *add_planetoid(char *s);

static void RenderScene(void);
static void ProcessKeys(unsigned char key, int x, int y);
static void ProcessSpecialKeys(int key, int x, int y);
static void ProcessMouseDrag(int x, int y);
static void ProcessMouseClicks(int button, int state, int x, int y);
void ProcessResize(int w, int h);

double stardate();

int screen_width, screen_height, screen_pixelDepth, screen_refreshRate;

GLuint poolwater, granite, wood, brick, cinderblock, borg;

int draw_counter = 0;

char *name = "Mercury";
double latitude = 33.5;
double longitude = -118.0;

GLuint surface_texture;

double lastx, lasty;  
GLfloat ambient[4]  = {0.5,0.5,0.5,0.5};
GLfloat diffuse[4]  = {1.0,1.0,1.0,0.8};
GLfloat specular[4] = {1.0,1.0,1.0,0.8};
GLfloat emission[4] = {0.5,0.5,0.5,0.5};

double start_time;

int main(int argc, char *argv[]) {
    glutInit( &argc, argv );
    glutInitDisplayMode( GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH );
    #ifdef MACOSX
    glutGameModeString( "1680x1050:24@60" );
    #else
    glutGameModeString( "1600x900:24@60" );
    #endif

    start_time = stardate();

    screen_width       = glutGameModeGet( GLUT_GAME_MODE_WIDTH );
    screen_height      = glutGameModeGet( GLUT_GAME_MODE_HEIGHT );
    screen_pixelDepth  = glutGameModeGet( GLUT_GAME_MODE_PIXEL_DEPTH );
    screen_refreshRate = glutGameModeGet( GLUT_GAME_MODE_REFRESH_RATE );

    glutInitWindowSize(900, 600);
    glutCreateWindow(name);
    /*glutEnterGameMode();*/

    
    glutDisplayFunc(RenderScene);
    glutIdleFunc(RenderScene);
    glutKeyboardFunc(ProcessKeys);
    glutSpecialFunc(ProcessSpecialKeys);
    glutMouseFunc(ProcessMouseClicks);
    glutMotionFunc(ProcessMouseDrag);
    glutReshapeFunc(ProcessResize);



    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    load_texture(&surface_texture);

    /* Camera */
    gluPerspective(90, 1680.0f / 1050.0f, 1.0, 2000); /* FOV, aspect, zNear, zFar */
    glMatrixMode(GL_MODELVIEW);

    glLoadIdentity();
    // gluLookAt(eyeX , eyeY , eyeZ , centerX , centerY , centerZ , upX , upY , upZ )

    gluLookAt(0.0, 12.0, 0.01,   // eyeX , eyeY , eyeZ
              0.0, 0.0, 0.0,      // centerX , centerY , centerZ
              0.0, 1.0, 0.0);     // upX , upY , upZ

    glutMainLoop();

    return 0;
}


void draw_mercury(double latitude, double longitude) {
  double now = stardate();

  GLUquadric *sphere;

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glScalef(-1.0,1.0,1.0); /* reverse the texture */

  glRotated( 180.0 +latitude, 1.0, 0.0, 0.0);
  glRotated( 180.0, 0.0, 1.0, 0.0);
  glRotated(-longitude + 1.0*(now-start_time), 0.0, 0.0, 1.0);
  
  glPolygonMode( GL_FRONT_AND_BACK, GL_FILL ); // GL_LINE or GL_FILL
  glBindTexture(GL_TEXTURE_2D, surface_texture);

  glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT,  ambient);
  glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE,  diffuse);
  glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular);
  glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, emission);
  glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 80);


  sphere = gluNewQuadric();
  glMatrixMode(GL_TEXTURE);
  gluQuadricTexture(sphere, GLU_TRUE); /* generate texture coordinates */
  glMatrixMode(GL_MODELVIEW);

  gluQuadricOrientation(sphere, GLU_OUTSIDE);
  gluQuadricNormals(sphere, GLU_SMOOTH); /* GLU_FLAT or GLU_SMOOTH */
  gluQuadricDrawStyle(sphere, GLU_FILL);
  gluSphere(sphere, 10.0, 360, 360);

  gluDeleteQuadric(sphere);

  glPopMatrix();
}


void make_lights() {
  GLfloat light_position[4]  = { 20000.0, 25000.0, 10.0, 0.0  }; //1000.0f, 1000.0f, -1000.0f, 1.0f };
  GLfloat light_direction[4] = { 0.0f, 0.0f, 0.0f  };
  GLfloat light_ambient[4]   = { 0.01f, 0.01f, 0.01f, 0.01f };
  GLfloat light_diffuse[4]   = { 0.1f, 0.1f, 0.1f, 0.1f };
  GLfloat light_specular[4]  = { 0.8f, 0.6f, 0.6f, 0.1f }; // yellowish sun
  GLfloat light_emission[4]  = { 0.1f, 0.1f, 0.1f, 0.1f };

  glEnable(GL_LIGHTING);
  glLightModeli(GL_LIGHT_MODEL_COLOR_CONTROL,GL_SEPARATE_SPECULAR_COLOR);

  // Lighting
  glLightfv(GL_LIGHT0, GL_POSITION,  light_position);
  glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, light_direction);
  glLightfv(GL_LIGHT0, GL_AMBIENT,   light_ambient);
  glLightfv(GL_LIGHT0, GL_DIFFUSE,   light_diffuse);
  glLightfv(GL_LIGHT0, GL_SPECULAR,  light_specular);
  glLightfv(GL_LIGHT0, GL_EMISSION,  light_emission);
  glLighti(GL_LIGHT0,  GL_SHININESS, 50); /* 0-128 */
  glLighti(GL_LIGHT0,  GL_SPOT_EXPONENT,  0);
  glLighti(GL_LIGHT0,  GL_SPOT_CUTOFF,  180); /* 0-90, or 180 */
  glEnable(GL_LIGHT0);
}

double stardate() {
  struct timeval tv;

  gettimeofday(&tv, NULL);
  return tv.tv_sec + (0.000001 * tv.tv_usec);
}

static void RenderScene(void) {
  make_lights();

  /* Okay, render the scene: */

  if(1) {
    glEnable(GL_SCISSOR_TEST);
    glScissor(0,screen_height-50, screen_width,screen_height);

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();

    glClearColor(0.2f, 0.2f, 0.4f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);

    glPopMatrix();
  }

  glEnable(GL_SCISSOR_TEST);
  glScissor(0,0, screen_width,screen_height-50);
  glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glEnable(GL_DEPTH_TEST);

  glEnable(GL_ALPHA_TEST);
  glAlphaFunc(GL_GEQUAL, 0.50);

  glLineWidth(1.0f);
  
  glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );

  glEnable(GL_TEXTURE_2D);

  draw_mercury(latitude, longitude);

  glutSwapBuffers();
}

int mouse_dragging = 0, drag_from_pos_x, y, drag_from_pos_y;

void ProcessResize(int w, int h) {
  /* Enforce a 0.60 window ratio */
  glViewport(0, 0, (GLsizei) w, (GLsizei) (w * 0.60));
  glutReshapeWindow( (GLsizei) w, (GLsizei) (w * 0.60) );
}

static void ProcessMouseDrag(int x, int y) {
  /* printf("ProcessMouseDrag(x=%d, y=%d)\n", x, y);*/
  if(mouse_dragging) {
     printf("Dragging: x=%d, drag_from_pos_x=%d ... y=%d, drag_from_pos_y=%d\n",
      x, drag_from_pos_x, y, drag_from_pos_y); 
  }
}

static void ProcessMouseClicks(int button, int state, int x, int y) {
  if(GLUT_ACTIVE_CTRL) {
    if( button == GLUT_LEFT_BUTTON ) {
      if(state == GLUT_UP) {  /*ctrl-left-click: rotate between fonts */
      }
    }
  }
}

static void ProcessSpecialKeys(int key, int x, int y) {
  int mods; /* key modifiers mask: GLUT_ACTIVE_SHIFT, GLUT_ACTIVE_CTRL, GLUT_ACTIVE_ALT */

  mods = glutGetModifiers();
  
  if(key == GLUT_KEY_END) {
        glutLeaveGameMode();
        exit(1);
  }

  if(mods & GLUT_ACTIVE_CTRL) { /* if CTRL was held down at the time */
    switch(key) {
      case GLUT_KEY_F1:
        break;
      default:
        break;
    }
  }
}

static void ProcessKeys(unsigned char key, int x, int y) {
  /* Process glut-collected keystroke */
  printf("ProcessKeys(key=%d, x=%d, y=%d)\n", key, x, y);
  if(key == 113) exit(0);
}



void load_texture(GLuint *texture_id) {
    struct jpeg_decompress_struct cinfo;
    JSAMPARRAY row_pointer[1];
    struct jpeg_error_mgr jerr;
    unsigned char *raster_buffer = NULL, *texture_buffer = NULL;
    int y;
    
    cinfo.err = jpeg_std_error(&jerr); /* this is necessary */
    jpeg_create_decompress(&cinfo);

    jpeg_mem_src(&cinfo, (unsigned char *)&mercury_jpg, mercury_jpg_len);
    jpeg_read_header(&cinfo, TRUE);
    jpeg_start_decompress(&cinfo);

    raster_buffer = malloc(cinfo.image_width * cinfo.num_components);

    texture_buffer = malloc(cinfo.image_width * cinfo.image_height * cinfo.num_components);

    for(y=0;y<cinfo.image_height;y++) {
      *row_pointer = (JSAMPARRAY)(texture_buffer + (y * cinfo.image_width * cinfo.num_components));
      jpeg_read_scanlines(&cinfo, (JSAMPARRAY)row_pointer, 1);
    }
                                                
    jpeg_finish_decompress(&cinfo);

    glGenTextures(1,texture_id);
    glBindTexture(GL_TEXTURE_2D, *texture_id);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, cinfo.image_width, cinfo.image_height, 1, GL_RGB, GL_UNSIGNED_BYTE, texture_buffer);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP);
    
    jpeg_destroy_decompress(&cinfo);
    free(raster_buffer);
    free(texture_buffer);
}
