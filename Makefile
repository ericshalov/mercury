#
#	Mercury
#	OpenGL simulation of orbit of planet Merury
#

CC = gcc
CCFLAGS = -Wall

all: mercury

mercury.o: mercury.c
	$(CC) $(CCFLAGS) -o mercury.o -c mercury.c

texture.h: mercury.jpg
	xxd -i mercury.jpg texture.h
	
mercury: mercury.o
	$(CC) $(CCFLAGS) -o mercury mercury.c -lGL -lGLU -lglut -ljpeg

clean:
	rm -f mercury mercury.o

